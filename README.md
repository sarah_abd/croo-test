# Project: Croo technical test  

Build a full app, client side, server and database  

## Description

We received the mandate to design a web application * Display user comments*, an app where users can write comments and should be persisted in a database, and displayed in real time for all users. 


## author

- Sarah Abdelaoui

## Supported platforms

This application was created and tested under the **macOS** platform

## Installation

The microframework used is Flask for Python based on Jinja2, it's used to simplify the use of HTTP

To run the application you must first install Flask

The installation of Flask is done using the command:

```
     pip3 install Flask
```

The Makefile script contains commands for the operation of this application.
The user must enter:

* The **make** command to run the _Makefile_ which is used to compile the project.

* The environment variable is defined in **the makefile**, this must be done before launching the application and is defined as follows:

```
    export FLASK_APP=index.py
```


## Commands and manipulations 
cd croo ui -> type: npm run eject
it configure the build process  
## Features requested and implemented



...

## Database

...

Deman features A database is located at the root of the project containing the `SQL` script

A creation of the database is necessary before using the application, in particular its manipulation and this is done
as shown in the following procedure:


```
    .... voir precedent
    .read db.sql
```

## Technologies used

Dans le **front-end**:


Dans le **back-end**:



## Project content
....



A database.py file 

An index.py file

A makefile file 

A README.md file




## Références

...

